# -*- coding: utf-8 -*-
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin


class HistoricalCreateModelMixin(CreateModelMixin):

	def create(self, request, *args, **kwargs):

		if request.user.is_authenticated:
			request.data['updated_by'] = request.user.id
		return super(HistoricalCreateModelMixin, self).create(request, *args, **kwargs)


class HistoricalUpdateModelMixin(UpdateModelMixin):

	def update(self, request, *args, **kwargs):
		if request.user.is_authenticated:
			request.data['updated_by'] = request.user.id
		return super(HistoricalUpdateModelMixin, self).update(request, *args, **kwargs)

	def partial_update(self, request, *args, **kwargs):
		if request.user.is_authenticated:
			request.data['updated_by'] = request.user.id
		return super(HistoricalUpdateModelMixin, self).partial_update(request, *args, **kwargs)
