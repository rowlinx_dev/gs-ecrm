# -*- coding: utf-8 -*-
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from base.views_mixins.mixins import HistoricalCreateModelMixin, HistoricalUpdateModelMixin


class ModelViewSet(
    HistoricalCreateModelMixin,
    RetrieveModelMixin,
    HistoricalUpdateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    GenericViewSet):
    pass


class NoCreateNoUpdateModelViewSet(
    RetrieveModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    GenericViewSet
):
    pass


class RetrieveModelViewSet(
    RetrieveModelMixin,
    ListModelMixin,
    GenericViewSet
):
    pass
