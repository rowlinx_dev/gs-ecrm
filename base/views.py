# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate
from django.contrib.auth.models import User, Group, Permission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from base.models import Historic
from base.serializer import UserSerializer, GroupSerializer, PermissionSerializer, HistoricSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['username', 'email', 'is_superuser']
    search_fields = ['username', 'email']
    ordering_fields = ['username', 'email']


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all().prefetch_related('permissions')
    serializer_class = GroupSerializer


class PermissionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows permissions to be viewed or edited.
    """
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer


class HistoricViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows historic to be viewed or edited.
    """
    queryset = Historic.objects.all()
    serializer_class = HistoricSerializer


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def logout(request):
    # simply delete the token to force a login
    request.user.auth_token.delete()
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request):
    data = request.data.copy()
    username = data['username']
    password = data['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        # A backend authenticated the credentials
        token = Token.objects.get_or_create(user=user)
        user_object = UserSerializer(user).data
        user_object.update({'all_permissions': user_all_permissions(user_id=user.id)})
        print(user_object)
        return Response({"token": token[0].key, "user": user_object})
    else:
        # No backend authenticated the credentials
        return Response({"error": "Please verify your username and password"})


def user_all_permissions(user_id):
    user = User.objects.get(id=user_id)
    permissions = Permission.objects.filter(user=user)
    for grp in Group.objects.filter(user=user):
        permissions |= Permission.objects.filter(group=grp)

    return [permission.codename for permission in permissions]


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def user_permissions(request, user_id: int, action: str):
    user = request.user
    data = request.data.copy()
    # TODO écrire un decorateur pour ce traitement
    if not user.has_perm('auth.change_user'):
        return Response(
            {"message": "You do not have the required rights for this action. Please contact the administrator"},
            status=status.HTTP_403_FORBIDDEN)

    try:
        user_obj = User.objects.get(pk=user_id)
        permissions = Permission.objects.filter(pk__in=data['permissions'])
        if action == 'add':
            user_obj.user_permissions.add(*permissions)

        elif action == 'update':
            user_obj.user_permissions.set(permissions)

        elif action == 'remove':
            user_obj.user_permissions.remove(*permissions)

        else:
            Response({"error": "Action not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({"success": "Action performed with success"}, status=status.HTTP_200_OK)
    except User.DoesNotExist:
        return Response({"error": "User don't exist"}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def user_groups(request, user_id: int, action: str):
    user = request.user
    data = request.data.copy()
    if not user.has_perm('auth.change_user'):
        return Response(
            {"message": "You do not have the required rights for this action. Please contact the administrator"},
            status=status.HTTP_403_FORBIDDEN)
    try:
        user_obj = User.objects.get(pk=user_id)
        groups = Group.objects.filter(pk__in=data['groups'])
        if action == 'add':
            user_obj.groups.add(*groups)

        elif action == 'update':
            user_obj.groups.set(groups)

        elif action == 'remove':
            user_obj.groups.remove(*groups)

        else:
            Response({"error": "Action not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({"success": "Action performed with success"}, status=status.HTTP_200_OK)
    except User.DoesNotExist:
        return Response({"error": "User don't exist"}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def group_permissions(request, group_id: int, action: str):
    user = request.user
    data = request.data.copy()
    if not user.has_perm('auth.change_group'):
        return Response(
            {"message": "You do not have the required rights for this action. Please contact the administrator"},
            status=status.HTTP_403_FORBIDDEN)

    try:
        group = Group.objects.get(pk=group_id)
        permissions = Permission.objects.filter(pk__in=data['permissions'])
        if action == 'add':
            group.permissions.add(*permissions)

        elif action == 'update':
            group.permissions.set(permissions)

        elif action == 'remove':
            group.permissions.remove(*permissions)

        else:
            Response({"error": "Action not found"}, status=status.HTTP_404_NOT_FOUND)

        return Response({"success": "Action performed with success"}, status=status.HTTP_200_OK)
    except Group.DoesNotExist:
        return Response({"error": "Group don't exist"}, status=status.HTTP_404_NOT_FOUND)


def handler404(request, *args, **kwargs):
    return Response({'message': 'This resource not found'}, status=status.HTTP_404_NOT_FOUND)


def handler500(request, *args, **kwargs):
    return Response({'message': 'An error has occurred on the server, please contact the administrator'},
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
