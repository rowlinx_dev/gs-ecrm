#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ast
import json
from rest_framework.authtoken.models import Token


class DefinedUpdatedBy(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        if request.headers.get('Authorization'):
            token = str(request.headers.get('Authorization')).split(' ')[1]
            user = Token.objects.get(key=token).user
            data = ast.literal_eval(request.body.decode("UTF-8"))
            data['update_by'] = user.id
            request.body = json.dumps(data).encode('utf-8')
        response = self.get_response(request)

        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        return
