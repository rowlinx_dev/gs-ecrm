#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from rest_framework import routers

from base.views import UserViewSet, PermissionViewSet, GroupViewSet, HistoricViewSet, login, logout, user_permissions, \
    user_groups, group_permissions

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'permission', PermissionViewSet)
router.register(r'group', GroupViewSet)
router.register(r'historic', HistoricViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^login/', login, name='login'),
    url(r'^logout/', logout, name='logout'),
    url(r'^user/(?P<user_id>[0-9]+)/permissions/(?P<action>[a-z]+)', user_permissions, name='user-permissions'),
    url(r'^user/(?P<user_id>[0-9]+)/groups/(?P<action>[a-z]+)', user_groups, name='user-groups'),
    url(r'^group/(?P<group_id>[0-9]+)/permissions/(?P<action>[a-z]+)', group_permissions, name='group-permissions'),
]
