#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.db import models


class Historic(models.Model):
    """management of the logging of action on the different tables of the application"""
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name="historic_ids"
    )
    type = models.CharField(
        max_length=30,
        choices=(
            ('create', 'Création'),
            ('edit', 'Modifiction'),
            ('delete', 'Suppression')))
    values = models.TextField(null=True, blank=True)
    new_values = models.TextField(null=True, blank=True)


class Base(models.Model):
    """ Management of the traceability of action on the system """

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)
    created_by = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_ownership", null=True
    )
    updated_by = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True,
        related_name="%(app_label)s_%(class)s_changeship"
    )

    def save(self, *args, **kwargs):
        """
            Surcharge de la modification pour la prise en compte de created_by et updated_by
            :param args:create
            :param kwargs:
            :return:
        """
        hist = Historic(type='edit', user_id=self.updated_by)
        if not self.created_by_id:
            hist.type = 'create'
            self.created_by = self.updated_by
        else:
            hist.values = serialize('json', [self, ])

        super(Base, self).save(*args, **kwargs)
        hist.new_values = serialize('json', [self, ])
        hist.save()

    # def delete(self, *args, **kwargs):
    #     """
    #     Gestion de la suppression et historisation
    #     :return:
    #     """
    #     hist = Historic(type='delete', group_id=self.updated_by, values=serialize('json', [self, ]))
    #     hist.save()
    #     super(Base, self).delete(*args, **kwargs)

    def activate(self):
        """
        activer une instance
        :return: objet self
        """
        self.active = True
        self.save()

    def disable(self):
        """
        desactiver l'objet en question
        :return: self
        """
        self.active = False
        self.save()

    class Meta:
        abstract = True
