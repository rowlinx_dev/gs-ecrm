# Generated by Django 3.0.5 on 2020-05-19 10:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Historic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('type', models.CharField(choices=[('create', 'Création'), ('edit', 'Modifiction'), ('delete', 'Suppression')], max_length=30)),
                ('values', models.TextField(blank=True, null=True)),
                ('new_values', models.TextField(blank=True, null=True)),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='historic_ids', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
