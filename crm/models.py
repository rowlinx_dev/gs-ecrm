#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

from base.models import Base


class Profile(Base):
    """Basic model for all members of congress, whether deputies or senators"""

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    matricule = models.CharField(max_length=50, unique=True)
    birth_date = models.DateTimeField(blank=True, null=True)
    birth_place = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    mobile = models.CharField(max_length=50, blank=True, null=True)
    is_salesman = models.BooleanField(default=False)

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)


class SalesTeam(Base):
    """Manage sales users"""

    name = models.CharField(max_length=50)
    salesmen = models.ManyToManyField(Profile)

    def __str__(self):
        return '%s' % self.name


class Client(Base):
    """"""

    name = models.CharField(max_length=150)
    birth_date = models.DateTimeField(blank=True, null=True)
    birth_place = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    mobile = models.CharField(max_length=50, blank=True, null=True)
    is_society = models.BooleanField(default=False)

    def __str__(self):
        return '%s' % self.name


class Opportunity(Base):
    """Gestion des opportunités"""

    name = models.CharField(max_length=200)
    client = models.ForeignKey(Client, related_name='opportunities', on_delete=models.CASCADE)
    salesman = models.ForeignKey(Profile, related_name='opportunities', on_delete=models.CASCADE)
    amount = models.FloatField()
    priority = models.CharField(max_length=30, choices=(('low', 'Faible'),
                                                        ('medium', 'Moyenne'),
                                                        ('high', 'Haute'),), default='medium')
    state = models.CharField(max_length=30, choices=(('suspect', 'Suspect'),
                                                     ('lead', 'Lead'),
                                                     ('prospect', 'Prospect'),
                                                     ('done', 'Client')), default='suspect')
    origin = models.CharField(max_length=100, choices=(('network', 'Réseaux soxiaux'),
                                                       ('web', 'Site web'),
                                                       ('campaign', 'Campaigne'),
                                                       ('other', 'Autre')))
    other_origin = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return '%s' % self.name


class Product(Base):
    """Products"""

    name = models.CharField(max_length=200)
    price_unit = models.FloatField()
    price_cost = models.FloatField(blank=True)
    product_type = models.CharField(max_length=30, choices=(('consumable', 'Consommable'),
                                                            ('service', 'Service')))

    def __str__(self):
        return '%s' % self.name


class Order(Base):
    """Devis"""

    reference = models.CharField(max_length=200)
    client = models.ForeignKey(Client, related_name='orders', on_delete=models.CASCADE)
    salesman = models.ForeignKey(Profile, related_name='orders', on_delete=models.CASCADE)
    opportunity = models.ForeignKey(Opportunity, on_delete=models.SET_NULL, null=True, related_name='orders')

    @property
    def total(self):
        total = 0
        order_lines = list(self.order_lines.values_list('price_unit', 'quantity'))
        for line in order_lines:
            total += line[0] * line[1]

        return total

    def __str__(self):
        return '%s' % self.reference


class OrderLine(Base):

    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_lines')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    quantity = models.FloatField()
    price_unit = models.FloatField()

    @property
    def amount(self):
        if self.price_unit and self.quantity:
            return self.quantity * self.price_unit
        else:
            return 0.0
