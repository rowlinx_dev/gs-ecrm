#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers

from .models import Profile, Client, Product, Opportunity, SalesTeam, Order, OrderLine


class ProfileSerializer(serializers.ModelSerializer):
    opportunities = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    orders = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Profile
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    opportunities = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    orders = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Client
        fields = '__all__'


class SalesTeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = SalesTeam
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'


class OpportunitySerializer(serializers.ModelSerializer):
    orders = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    client = serializers.StringRelatedField()
    salesman = serializers.StringRelatedField()

    class Meta:
        model = Opportunity
        fields = '__all__'


class OrderLineSerializer(serializers.ModelSerializer):
    amount = serializers.FloatField(read_only=True)
    order = serializers.StringRelatedField()
    product = serializers.StringRelatedField()

    class Meta:
        model = OrderLine
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    # client = serializers.
    total = serializers.FloatField(read_only=True)
    order_lines = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    client = serializers.StringRelatedField()
    salesman = serializers.StringRelatedField()
    opportunity = serializers.StringRelatedField(many=True)

    class Meta:
        model = Order
        fields = '__all__'
