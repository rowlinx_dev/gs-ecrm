#!/usr/bin/env python
# -*- coding: utf-8 -*-

from base.views_mixins.viewsets import ModelViewSet

from .models import Profile, Client, SalesTeam, Product, Opportunity, Order, OrderLine
from .serializer import ProfileSerializer, ClientSerializer, SalesTeamSerializer, ProductSerializer, \
    OpportunitySerializer, OrderSerializer, OrderLineSerializer


class ProfileViewSet(ModelViewSet):
    """API endpoint that allows deputy to be viewed or edited."""

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class ClientViewSet(ModelViewSet):

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class SalesTeamViewSet(ModelViewSet):

    queryset = SalesTeam.objects.all()
    serializer_class = SalesTeamSerializer


class ProductViewSet(ModelViewSet):

    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class OpportunityViewSet(ModelViewSet):

    queryset = Opportunity.objects.all()
    serializer_class = OpportunitySerializer


class OrderViewSet(ModelViewSet):

    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class OrderLineViewSet(ModelViewSet):

    queryset = OrderLine.objects.all()
    serializer_class = OrderLineSerializer
