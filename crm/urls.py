#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from rest_framework import routers

from .views import ProfileViewSet, ClientViewSet, SalesTeamViewSet, ProductViewSet, OpportunityViewSet, OrderViewSet, \
    OrderLineViewSet

router = routers.DefaultRouter()
router.register(r'profiles', ProfileViewSet)
router.register(r'contacts', ClientViewSet)
router.register(r'salesteams', SalesTeamViewSet)
router.register(r'products', ProductViewSet)
router.register(r'opportunities', OpportunityViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'orderlines', OrderLineViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
]
